﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Models
{
    public abstract class QueryStringParameters
    {
        //restrict our Api to maximum of 50 owners per Page
        const int maxPageSize = 50; //->for validation purpose
        public int PageNumber { get; set; } = 1;
        //amount of object per Page
        private int _pageSize = 10;
        public int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = (value > maxPageSize) ? maxPageSize : value;
            }
        }
    }
}

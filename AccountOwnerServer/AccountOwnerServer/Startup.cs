using AccountOwnerServer.Extensions;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AccountOwnerServer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            //Configuration for logger service
            LogManager.LoadConfiguration(String.Concat(Directory.GetCurrentDirectory(), "/nlog.config"));
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //add services from new exntensions:)
            services.ConfigureCors();
            services.ConfigureIISIntegration();

            //add Logger
            services.ConfigureLoggerService();

            //add Context service to the IOC(Inversion of Control). MySql(get from Extensions folder)
            services.ConfigureMySqlContext(Configuration);

            //add RepositoryWrapper
            services.ConfigureRepositoryWrapper();

            //add AutoMapper(registry this service)
            services.AddAutoMapper(typeof(Startup));

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            ////////////######
            ///enables using static files for the request.
            app.UseStaticFiles();
            ///from Extensions
            app.UseCors("CorsPolicy");

            ///will forward proxy headers to the current request(Linux depoyment)
            app.UseForwardedHeaders(new ForwardedHeadersOptions  
            {
                ForwardedHeaders = ForwardedHeaders.All
            });
            ///////////####
            ///
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

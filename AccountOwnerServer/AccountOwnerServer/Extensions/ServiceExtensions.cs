﻿using Contracts;
using LoggerService;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Repository;


namespace AccountOwnerServer.Extensions
{
    public static class ServiceExtensions
    {

        //CORS(Cross-Origin Resource Sharing) -  is a mechanism that gives rights to the user
        //to access resources from the server on a different domain.
        //Default implementatin
        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });
        }

        //configure an IIS integration which will help us with the IIS deployment
        //default implementation
        public static void ConfigureIISIntegration(this IServiceCollection services)
        {
            services.Configure<IISOptions>(options =>
            {
            });
        }

        public static void ConfigureLoggerService(this IServiceCollection services)
        {
            services.AddSingleton<ILoggerManager, LoggerManager>();
        }

        //Configuration MySql context
        public static void ConfigureMySqlContext(this IServiceCollection services, IConfiguration config)
        {
            //dirctives to MySql db
            var connectionString = config["mysqlconnection:connectionString"];
            //services.AddDbContext<Entities.RepositoryContext>(o => o.UseMySql(connectionString));

            services.AddDbContext<Entities.RepositoryContext>
                (o => o.UseMySql(connectionString, MySqlServerVersion.LatestSupportedServerVersion));
        }

        //repostory Wrapper
        public static void ConfigureRepositoryWrapper(this IServiceCollection services)
        {
            services.AddScoped<IRepositoryWrapper, RepositoryWrapper>();
        }

    }

    
}

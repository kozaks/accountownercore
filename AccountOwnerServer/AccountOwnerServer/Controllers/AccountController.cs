﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Entities.Extensions;


namespace AccountOwnerServer.Controllers
{
    [Route("api/accounts")]
    public class AccountController : ControllerBase
    {

        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        private IMapper _mapper;

        public AccountController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
        {
            _logger = logger;
            _repository = repository;
            _mapper = mapper;
        }

        /// <summary>
        /// Get Accounts by Id Owners
        /// http://localhost:5000/api/accounts?pageNumber=2&pageSize=2
        /// </summary>
        /// <param name="ownerId"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetAccountsForOwner(Guid ownerId, [FromQuery] AccountParameters parameters)
        {

            var accounts = _repository.Account.GetAccountsByOwner(ownerId, parameters);

            var metadata = new
            {
                accounts.TotalCount,
                accounts.PageSize,
                accounts.CurrentPage,
                accounts.TotalPages,
                accounts.HasNext,
                accounts.HasPrevious
            };

            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            _logger.LogInfo($"Returned {accounts.TotalCount} owners from database");

            return Ok(accounts);

        }


        //TODO
        //To implemented!!!!

        //[HttpGet("{id}")]
        //public IActionResult GetAccountForOwner(Guid ownerId, Guid id)
        //{
        //    var account = _repository.Account.GetAccountByOwner(ownerId, id);

        //    if (account.IsEmptyObject())
        //    {
        //        _logger.LogError($"Owner with id: {id}, hasn't been found in db.");
        //        return NotFound();
        //    }

        //    return Ok(account);
        //}







    }
}

﻿using AutoMapper;
using Contracts;
using Entities.DataTransferObjects;
using Entities.Helpers;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountOwnerServer.Controllers

{
    
    [Route("api/owners")]
    [ApiController]
    public class OwnerController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        private IMapper _mapper;


        public OwnerController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
        {
            _logger = logger;
            _repository = repository;
            _mapper = mapper;

        }


        /// <summary>
        /// Get Owners with pagination
        /// http://localhost:5000/api/owners?pageNumber=2&pageSize=2
        /// http://localhost:5000/api/owners?minYearOfBirth=1975
        /// http://localhost:5000/api/owners?minYearOfBirth=1975&maxYearOfBirth=1997
        /// http://localhost:5000/api/owners?name=Anna Bosh
        /// http://localhost:5000/api/owners?name=o
        /// http://localhost:5000/api/owners?name=o&minYearOfBirth=1974&maxYearOfBith=1985&pageSize=1&pageNumber=2
        /// </summary>
        /// <param name="ownerParameters"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetOwners([FromQuery] OwnerParameters ownerParameters)
        {
            
           
                if (!ownerParameters.ValidYearRange)
                {
                    return BadRequest("Max year of birth cannot be less than min year of birth ");
                }

                
               // var owners = _repository.Owner.GetOwners(ownerParameters);
                PagedList<Owner> owners = _repository.Owner.GetOwners(ownerParameters);

            var metadata = new
                {
                    owners.TotalCount,
                    owners.PageSize,
                    owners.CurrentPage,
                    owners.TotalPages,
                    owners.HasNext,
                    owners.HasPrevious
                };

                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
                _logger.LogInfo($"Returned {owners.TotalCount} owners from database");
                return Ok(owners);
            
         

        }

        ////http://localhost:5000/api/owners
        //[HttpGet]
        //public IActionResult GetAllOwners()
        //{
        //    try
        //    {
        //        var owners = _repository.Owner.GetAllOwners();
        //        _logger.LogInfo($"Returned all owners from database.");

        //        var ownersResult = _mapper.Map<IEnumerable<OwnerDto>>(owners);
        //        return Ok(owners);
        //    }
        //    catch(Exception ex)
        //    {
        //        _logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
        //        return StatusCode(500, "Internal server error");


        //    }
        //}

        //http://localhost:5000/api/owners/IdOwner
        [HttpGet("{id}", Name = "OwnerById")]
        public IActionResult GetOwnerById(Guid id)
        {
            try
            {
                var owner = _repository.Owner.GetOwnerById(id);
                if(owner == null)
                {
                    _logger.LogError($"Owner with id: {id}, hasn't been found in db.");
                    return NotFound();

                }
                else
                {
                    _logger.LogInfo($"Owner with id: {id}");

                    var ownerResult = _mapper.Map<OwnerDto>(owner);
                    return Ok(ownerResult);
                }
            }
            catch(Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetOwnerById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        //http://localhost:5000/api/owner/IdOwners/account
        [HttpGet("{id}/account")]
        public IActionResult GetOwnerWithDetails(Guid id)
        {
            try
            {
                var owner = _repository.Owner.GetOwnerWithDetails(id);

                if(owner == null)
                {
                    _logger.LogError($"Owner with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned owner with details for id: {id}");

                    var ownerResult = _mapper.Map<OwnerDto>(owner);
                    return Ok(ownerResult);
                }
            }
            catch(Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetOwnerWithDetails action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        //http://localhost:5000/api/owners
        [HttpPost]
        public IActionResult CreateOwner([FromBody] OwnerForCreationDto owner)
        {
            try
            {
                if (owner == null)
                {
                    _logger.LogError("Owner object sent from client is null.");
                    return BadRequest("Owner object is null");
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid owner object sent from client.");
                    return BadRequest("Invalid model object");
                }

                //mapowanie OwnerForCreationDto na Owner'a(data base entity)
                var ownerEntity = _mapper.Map<Owner>(owner);

                _repository.Owner.CreateOwner(ownerEntity);
                _repository.Save();

                //mapowanie Owner'a na OwnerDto i wyswietlenie informacji uzytkownikowi
                var createdOwner = _mapper.Map<OwnerDto>(ownerEntity);

                //return status code 201 -> it mean Created!
                return CreatedAtRoute("OwnerById", new { Id = createdOwner.OwnerId }, createdOwner);
               
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        //http://localhost:5000/api/owners/IdOwner
        [HttpPut("{id}")]
        public IActionResult UpdateOwner(Guid id, [FromBody] OwnerForUpdateDto owner)
        {
            try
            {
                if (owner == null)
                {
                    _logger.LogError("Owner object sent from client is null.");
                    return BadRequest("Owner object is null");
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid owner object sent from client.");
                    return BadRequest("Invalid model object");
                }

                var ownerEntity = _repository.Owner.GetOwnerById(id);
                if (ownerEntity == null)
                {
                    _logger.LogError($"Owner with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _mapper.Map(owner, ownerEntity);

                _repository.Owner.UpdateOwner(ownerEntity);
                _repository.Save();

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteOwner(Guid id)
        {
            //try
            //{
            //    var owner = _repository.Owner.GetOwnerById(id);
            //    if(owner == null)
            //    {
            //        _logger.LogError($"Owner with id: {id}, hasn't been found in db.");
            //        return NotFound();
            //    }

            //    if (_repository.Account.AccountsByOwner(id).Any())
            //    {
            //        _logger.LogError($"Cannot delete owner with id: {id}. It has related accounts. Delete those accounts first");
            //        return BadRequest("Cannot delete owner. It has related accounts. Delete those accounts first");
            //    }

            //    _repository.Owner.DeleteOwner(owner);
            //    _repository.Save();

            //    return NoContent();
            //}
            //catch(Exception ex)
            //{
            //    _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
            //    return StatusCode(500, "Internal server error");

            //}
            try
            {
                var owner = _repository.Owner.GetOwnerById(id);
                if (owner == null)
                {
                    _logger.LogError($"Owner with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                _repository.Owner.DeleteOwner(owner);
                _repository.Save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


    }
}
